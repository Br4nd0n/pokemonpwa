import React from 'react';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import './App.css';
import { Button, Grid } from '@mui/material';
import { styled } from '@mui/material/styles';
import { Link } from 'react-router-dom';

export function PokemonList(props) {
  const { list, setDetails } = props;

  const PokemonListWrapper = styled('div')(({ theme }) => ({
    padding: theme.spacing(2),
  }));

  return (
    <PokemonListWrapper>
      <Grid
        container
        spacing={{ xs: 1, sm: 2, md: 4, lg: 4 }}
        columns={{ xs: 4, sm: 8, md: 12, lg: 16 }}
      >
        {list.map((item) => {
          return (
            <Grid item xs={4} sm={4} md={4} key={item.name}>
              <>
                <Card sx={{ maxWidth: 345 }}>
                  <CardMedia
                    component="img"
                    height="140"
                    width="140"
                    image={item.sprites.front_default}
                    alt={item.name}
                  />
                  <CardContent>
                    <Typography gutterBottom variant="h5" component="div">
                      {item.name}
                    </Typography>
                    <Typography variant="body2" color="text.secondary">
                      {item.url}
                    </Typography>
                  </CardContent>
                  <CardActions>
                    <Link
                      to="/details"
                      style={{ textDecoration: 'none' }}
                      onClick={() => setDetails(item)}
                    >
                      <Button variant="outlined">Details</Button>
                    </Link>
                  </CardActions>
                </Card>
              </>
            </Grid>
          );
        })}
      </Grid>
    </PokemonListWrapper>
  );
}
