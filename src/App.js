import './App.css';
import React, { useState } from 'react';
import { Header } from './Header';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import { PokemonDetails } from './PokemonDetails';
import { Main } from './Main';

function App() {
  const [selectedDetails, setDetails] = useState({});

  return (
    <>
      <Header />
      <BrowserRouter>
        <Routes>
          <Route exact path="/" element={<Main setDetails={setDetails} />}></Route>
          <Route path="/details" element={<PokemonDetails details={selectedDetails} />}></Route>
        </Routes>
      </BrowserRouter>
    </>
  );
}

export default App;
