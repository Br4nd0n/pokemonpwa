import './App.css';
import React, { useEffect, useState } from 'react';
import { PokemonList } from './PokemonList';
import { Button } from '@mui/material';
import { styled } from '@mui/material/styles';

export function Main(props) {
  const [meta, setMeta] = useState();
  const [list, setList] = useState([]);
  const [page, setPage] = useState(1);
  const [url, setUrl] = useState('https://pokeapi.co/api/v2/pokemon');
  const { setDetails } = props;

  useEffect(() => {
    fetch(url)
      .then((res) => res.json())
      .then((data) => {
        const metaResult = {
          count: data.count,
          next: data.next,
          previous: data.previous,
        };
        setMeta(metaResult);
        const detailsUrls = data.results.map(({ url }) => url);
        //console.log(detailsUrls);
        Promise.all(detailsUrls.map((details) => fetch(details)))
          .then((results) => Promise.all(results.map((r) => r.json())))
          .then((data) => {
            setList(data);
          });
      });
  }, [page, url]);
  const handleNextClicked = () => {
    setUrl(meta.next);
    setPage(page + 1);
  };
  const handlePreviousClicked = () => {
    setUrl(meta.previous);
    setPage(page - 1);
  };
  const ButtonWrapper = styled('div')(({ theme }) => ({
    padding: theme.spacing(3, 3, 3, 0),
    float: 'right',
  }));

  return (
    <>
      <PokemonList list={list} setDetails={setDetails} />
      <ButtonWrapper>
        <Button
          onClick={handlePreviousClicked}
          variant="outlined"
          disabled={meta && meta.previous === null}
          style={{ marginRight: 10 }}
        >
          {' '}
          previous
        </Button>
        <Button onClick={handleNextClicked} variant="outlined">
          {' '}
          Next
        </Button>
      </ButtonWrapper>
    </>
  );
}
