import { Button } from '@mui/material';
import { Link } from 'react-router-dom';
import { styled } from '@mui/material/styles';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';
import Divider from '@mui/material/Divider';
import { Grid } from '@mui/material';
import Chip from '@mui/material/Chip';
import { useState } from 'react';
import TextField from '@mui/material/TextField';

export function PokemonDetails(props) {
  const { details } = props;
  const [url, setUrl] = useState();

  const PokemonDetailsWrapper = styled('div')(({ theme }) => ({
    padding: theme.spacing(2),
  }));

  function backgroundSync() {
    navigator.serviceWorker.ready
      .then((swRegistration) => swRegistration.sync.register('post'))
      .catch(console.log);
  }
  const handlePost = () => {
    console.log('url : ', url);
    const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*' },
      body: JSON.stringify({ details }),
    };
    fetch(url, requestOptions)
      .then((response) => console.log('response ', response))
      .catch(() => backgroundSync());
  };

  return (
    <PokemonDetailsWrapper>
      <main style={{ padding: '1rem 0' }}>
        <div>
          <Link to="/" style={{ textDecoration: 'none', float: 'left' }}>
            <Button variant="contained">Back</Button>
          </Link>
          <div style={{ float: 'right' }}>
            <TextField
              variant="outlined"
              label="Post Endpoint"
              onChange={(e) => setUrl(e.target.value)}
            ></TextField>
            <Button variant="outlined" onClick={handlePost}>
              Post Details
            </Button>
          </div>
        </div>
        <h1 style={{ textAlign: 'center' }}>{details.name}'s Details</h1>

        <Grid
          container
          spacing={{ xs: 1, sm: 2, md: 4, lg: 4 }}
          columns={{ xs: 4, sm: 8, md: 8, lg: 10 }}
        >
          <Grid item xs={4} sm={4} md={4} key="image">
            <img
              src={details.sprites.front_default}
              alt={details.name}
              style={{ height: 20 + 'em', width: 20 + 'em' }}
            />
          </Grid>
          <Grid item xs={4} sm={4} md={6} key="details">
            <List
              sx={{
                width: '100%',
                bgcolor: 'background.paper',
              }}
              dense={true}
            >
              <Divider>
                <Chip label="Types" />
              </Divider>
              {details.types &&
                details.types.map((a) => (
                  <ListItem>
                    <ListItemText secondary={a.type.name} />
                  </ListItem>
                ))}
              <Divider>
                <Chip label="Stats" />
              </Divider>
              {details.stats &&
                details.stats.map((a) => (
                  <ListItem>
                    <ListItemText secondary={`${a.stat.name}: ${a.base_stat}`} />
                  </ListItem>
                ))}
            </List>
          </Grid>
        </Grid>
        <Divider>
          <Chip label="Attacks" />
        </Divider>
        <List
          sx={{
            width: '100%',
            bgcolor: 'background.paper',
          }}
          dense={true}
        >
          <ListItem>
            <Grid
              container
              spacing={{ xs: 1, sm: 2, md: 4, lg: 4 }}
              columns={{ xs: 4, sm: 8, md: 12, lg: 16 }}
            >
              {details.moves &&
                details.moves.map((m) => {
                  return <ListItemText style={{ margin: 2.5 + 'em' }} secondary={m.move.name} />;
                })}
            </Grid>
          </ListItem>
        </List>
      </main>
    </PokemonDetailsWrapper>
  );
}
